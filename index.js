var echoshow = require('./echoshow.js');
var echodot = require('./echodot.js');
const Alexa = require('alexa-sdk');


exports.handler = function (event, context, callback) {
        if (event.context.System.device.supportedInterfaces.Display) {
                console.log("inside echo show");
                context.succeed(echoshow(event, context, callback));
        }
        else {
                console.log("inside echo dot");
                context.succeed(echodot(event, context, callback));
        }
}