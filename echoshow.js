"use strict";
/* const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
const template = builder.setBackgroundImage(makeImage('https://s3.amazonaws.com/kidscodegame/Scenes/screen1.png'))
.setBackButtonBehavior('HIDDEN')
.build(); 
            .renderTemplate(template)
*/
const Alexa = require('alexa-sdk');
var images = require('./images.js');
var responses = require('./arrays.js');
const makeImage = Alexa.utils.ImageUtils.makeImage

module.exports = function (event, context, callback) {
      const alexa = Alexa.handler(event, context);
        alexa.registerHandlers(handlers, gameStartHandler, FirstinningsHandler, SecondinningsHandler, PostmatchHandler);
        alexa.appId = 'amzn1.ask.skill.3d838f3c-e551-4630-ba73-574afe9ee104'; // APP_ID is your skill id which can be found in the Amazon developer console where you create the skill.
        callback(null, JSON.stringify(alexa.execute()));
    };


    const handlers = {
        "LaunchRequest": function () {
            console.log("launch Request ");
            this.handler.state = states.GAMESTART;
            this.emitWithState("Start");
        }
    }

// States of the Game
const states = {
    GAMESTART: "_GAMESTART",
    FIRSTINNINGS: "_FIRSTINNINGS",
    SECONDINNINGS: "_SECONDINNINGS",
    POSTMATCH: "_POSTMATCH"
};

//Default Handler

// Game Started
const gameStartHandler = Alexa.CreateStateHandler(states.GAMESTART, {
    "LaunchRequest": function () {
        this.emitWithState('Start');
    },
    "Start": function () {
        var imagepre = images.imagepre();
        console.log(imagepre);
        const builder = new Alexa.templateBuilders.BodyTemplate1Builder();
        const template = builder.setBackgroundImage(makeImage(imagepre))
            .setBackButtonBehavior('HIDDEN')
            .build();
        this.response.renderTemplate(template)
            .speak('Hello, Welcome to Book Cricket. Shall We Start the Game?')
            .listen('say yes to toss')
            .shouldEndSession(false);
        this.emit(':responseReady');
    },
    'TossIntent': function () {
        if (!this.attributes['Tossed']) {
            let a = Math.floor(Math.random() * 2);
            let outcome;
            if (a < 1) {
                outcome = "heads";
            } else {
                outcome = "tails";
            }
            this.attributes['Tossed'] = outcome;
            const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
            var imagetoss = images.imagetoss();;
            const template = builder.setBackgroundImage(makeImage(imagetoss))
                .setBackButtonBehavior('HIDDEN')
                .build();
            this.response.renderTemplate(template)
                .speak('Please choose Heads or Tails. The one with the right outcome Bats first')
                .listen('Say heads or tails to continue');
            this.emit(':responseReady');
        }
        else {
            if (this.attributes.Tossed == this.event.request.intent.slots.Toss.value) {
                const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
                var imagetoss = images.imagetoss();;
                const template = builder.setBackgroundImage(makeImage(imagetoss))
                    .setBackButtonBehavior('HIDDEN')
                    .build();
                this.response.renderTemplate(template)
                    .speak('you won toss, say yes to bat')
                    .listen('you won toss, say yes to bat')
                    .shouldEndSession(false);
                this.emit(':responseReady');
            }
            else if (this.attributes.Tossed != this.event.request.intent.slots.Toss.value) {
                const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
                var imagetoss = images.imagetoss();;
                const template = builder.setBackgroundImage(makeImage(imagetoss))
                    .setBackButtonBehavior('HIDDEN')
                    .build();
                this.response.renderTemplate(template)
                    .speak('you lost toss, you will be bowling now, say yes to continue')
                    .listen('you lost toss, you will be bowling now, say yes to continue')
                    .shouldEndSession(false);
                this.emit(':responseReady');
            }
        }
    },
    'Unhandled': function () {
        this.emit(':ask', "Please say yes to resume game", "You need to answer with a yes or no");
    },

    'AMAZON.NoIntent': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },


    'AMAZON.YesIntent': function () {
        if (!this.attributes['Tossed']) {
            this.emitWithState('TossIntent');
        }
        else {
            this.handler.state = states.FIRSTINNINGS;
            this.emitWithState('Start');
        }
    },

    'AMAZON.HelpIntent': function () {
        this.response.speak('Lets have a game of book cricket, say yes to start the game')
            .listen('say yes to start the game');
        this.emit(':responseReady');
    },

    'SessionEndedRequest': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);

        this.emit(':responseReady');
    },

    'AMAZON.StopIntent': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    'AMAZON.CancelIntent' : function (){
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    }, 
});
// First Innings
const FirstinningsHandler = Alexa.CreateStateHandler(states.FIRSTINNINGS, {
    "LaunchRequest": function () {
        this.emitWithState('Start');
    },
    "Start": function () {
        var answer = Math.floor(Math.random() * 7);
        var message;
        var imaged;
        if (!this.attributes['batsmenscore']) {
            var batsmenscore = 0;
        }
        else {
            var batsmenscore = this.attributes.batsmenscore;
        }
        if (!this.attributes['i']) {
            var i = 0;
        }
        else {
            var i = this.attributes.i;
        }
        if (!this.attributes['Total']) {
            var Total = 0;
        }
        else {
            var Total = this.attributes.Total;
        }
        console.log(answer);
        if (i < 2) {
            batsmenscore = batsmenscore + answer;
            Total = Total + answer;

            if (answer == 0) {
                message = responses.answernil();;
                imaged = images.imagenil();;
            }
            if (answer == 1) {
                message = responses.answer1();;
                imaged = images.image1();;
            }
            if (answer == 2) {
                message = responses.answer2();;
                imaged = images.image2();;
            }
            if (answer == 3) {
                message = responses.answer3();;
                imaged = images.image3();;
            }
            if (answer == 4) {
                message = responses.answer4();;
                imaged = images.image4();;
            }
            if (answer == 5) {
                message = responses.answer5();;
                imaged = images.image5();;
            }
            if (answer == 6) {
                message = responses.answer6();;
                imaged = images.image6();;
            }
            console.log(message);
            this.attributes.batsmenscore = batsmenscore;
            this.attributes.Total = Total;
            this.attributes.i = i;
            const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
            const template = builder.setBackgroundImage(makeImage(imaged))
                .setBackButtonBehavior('HIDDEN')
                .build();
            this.response.renderTemplate(template)
                .speak(message + '.  Please say the score for next ball.')
                .listen('say yes for next ball');
            this.emit(':responseReady');
        }

        else {
            this.attributes.i = i;
            console.log('end of your innings');
            message = 'You have scored a total runs of ' + Total;
            console.log(message);
            const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
            var over = images.end(); ;
            const template = builder.setBackgroundImage(makeImage(over))
                .setBackButtonBehavior('HIDDEN')
                .build()
            this.response.renderTemplate(template)
                .speak(message + '. Say yes for next innings')
                .listen('say yes for next innings');
            this.emit(':responseReady');
        }
    },

    'AMAZON.NoIntent': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':saveState', true);
        this.emit(':responseReady');
    },

    'AMAZON.YesIntent': function () {
        var k = this.attributes.i;
        if (k == 2) {
            this.handler.state = states.SECONDINNINGS;
            this.emitWithState('Start');
        }
        else {
            this.emitWithState('Start');
        }

    },
    'Unhandled': function () {
        var Total = this.attributes.Total;
        this.emit(':ask', "Please say score to resume game. The score is" + Total, "You need to answer with a number");
    },
    'ScoreIntent': function () {
        if (this.event.request.intent.slots.Number) {
            if (this.attributes.Total == this.event.request.intent.slots.Number.value) {
                this.emitWithState('Start');
            }
            else {
                var i = this.attributes.i;
                if (i < 2) {
                    var batsmenscore = this.attributes.batsmenscore;
                    var i = this.attributes.i;
                    i++;
                    var message = responses.answer0(); + ". Batsmen " + i + " scored " + batsmenscore + " runs"
                    var batsmenscore = 0;
                    this.attributes.batsmenscore = batsmenscore;
                    this.attributes.i = i;
                    var Total = this.attributes.Total;
                    if (i <= 1) {
                        const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
                        var imaged = images.imageout(); ;
                        const template = builder.setBackgroundImage(makeImage(imaged))
                            .setBackButtonBehavior('HIDDEN')
                            .build();
                        this.response.renderTemplate(template)
                            .speak(message + ". The score is " + Total + ". You are  " + i + " batsmen down. Please say the score for next batsmen")
                            .listen('');
                        this.emit(':responseReady');
                    }
                    else {
                        var imaged = images.imageout();;
                        const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
                        const template = builder.setBackgroundImage(makeImage(imaged))
                            .setBackButtonBehavior('HIDDEN')
                            .build();
                        this.response.renderTemplate(template)
                            .speak(message + " The score is " + Total + ". You are " + i + " batsmen down. Please say score to end your innings")
                            .listen('');
                        this.emit(':responseReady');
                    }
                } else {
                    var Total = this.attributes.Total;
                    this.response.speak('The score is ' + Total + ' Please say the score to continue')
                        .listen('')
                    this.emit(':responseReady');
                }
            }
        }
        else {
            this.response.speak("say the score")
                .listen(' ');
            this.emit(':responseReady');
        }
    },
    'AMAZON.HelpIntent': function () {
        this.response.speak('Lets have a game of book cricket, say yes to start the game')
            .listen('say yes to start the game');
        this.emit(':responseReady');
    },

    'AMAZON.StopIntent': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    'AMAZON.CancelIntent' : function (){
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    }, 

    'SessionEndedRequest': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    }
});
//SecondInnings

const SecondinningsHandler = Alexa.CreateStateHandler(states.SECONDINNINGS, {
    "LaunchRequest": function () {
        this.emitWithState('Start');
    },
    "Start": function () {
        var imaged;
        var F = this.attributes.Total;
        if (!this.attributes['Total2']) {
            var Total2 = 0;
        }
        else {
            var Total2 = this.attributes.Total2;
        }
        if (F >= Total2) {
            var answer = Math.floor(Math.random() * 7);
        } else {
            this.handler.state = states.POSTMATCH;
            this.emitWithState('Start');
        }
        var message;
        if (!this.attributes['batsmenscore']) {
            var batsmenscore = 0;
        }
        else {
            var batsmenscore = this.attributes.batsmenscore;
        }
        if (!this.attributes['r']) {
            var r = 0;
        }
        else {
            var r = this.attributes.r;
        }
        console.log(answer);
        if (r < 2) {
            batsmenscore = batsmenscore + answer;
            Total2 = Total2 + answer;
            if (answer == 0) {
                message = responses.answernil(); ;
                imaged = images.imagenil(); ;
            }
            if (answer == 1) {
                message = responses.answer1(); ;
                imaged = images.image1(); ;
            }
            if (answer == 2) {
                message = responses.answer2(); ;
                imaged = images.image2(); ;
            }
            if (answer == 3) {
                message = responses.answer3(); ;
                imaged = images.image3(); ;
            }
            if (answer == 4) {
                message = responses.answer4(); ;
                imaged = images.image4(); ;
            }
            if (answer == 5) {
                message = responses.answer5(); ;
                imaged = images.image5(); ;
            }
            if (answer == 6) {
                message = responses.answer6(); ;
                imaged = images.image6(); ;
            }
            console.log(message);
            this.attributes.batsmenscore = batsmenscore;
            this.attributes.Total2 = Total2;
            this.attributes.r = r;
            const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
            const template = builder.setBackgroundImage(makeImage(imaged))
                .setBackButtonBehavior('HIDDEN')
                .build();
            this.response.renderTemplate(template)
                .speak(message + '. Say score for next ball.')
                .listen('say score for next ball');
            this.emit(':responseReady');

        }
        else {
            this.handler.state = states.POSTMATCH;
            this.emitWithState('Start');
        }
    },
    'Unhandled': function () {
        var Total2 = this.attributes.Total2;
        this.emit(':ask', "Please say score to resume game. The score is" + Total2, "You need to answer with a score");
    },

    'ScoreIntent': function () {
        if (this.event.request.intent.slots.Number) {
            if (this.attributes.Total2 == this.event.request.intent.slots.Number.value) {
                this.emitWithState('Start');
            } else {
                var r = this.attributes.r;
                if (r < 2) {
                    var batsmenscore = this.attributes.batsmenscore;
                    var r = this.attributes.r;
                    r++;
                    var message = responses.answer0(); +". Batsmen " + r + " scored" + batsmenscore + " runs"
                    var batsmenscore = 0;
                    this.attributes.batsmenscore = batsmenscore;
                    this.attributes.r = r;
                    var Total2 = this.attributes.Total2;
                    var imaged = images.imageout(); ;

                    if (r <= 1) {
                        const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
                        const template = builder.setBackgroundImage(makeImage(imaged))
                            .setBackButtonBehavior('HIDDEN')
                            .build();
                        this.response.renderTemplate(template)
                            .speak(message + " The score is " + Total2 + " You are " + r + " batsmen down. Please say the score for next batsmen")
                            .listen('');
                        this.emit(':responseReady');
                    } else {
                        const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
                        const template = builder.setBackgroundImage(makeImage(imaged))
                            .setBackButtonBehavior('HIDDEN')
                            .build();
                        this.response.renderTemplate(template)
                            .speak(message + " The score is " + Total2 + " You are " + r + " batsmen down. Please say the score to end your innings")
                            .listen('');
                        this.emit(':responseReady');
                    }
                } else {
                    var Total2 = this.attributes.Total2;
                    this.response.speak('The score is ' + Total2 + ' . Please say the score to continue')
                        .listen('')
                    this.emit(':responseReady');
                }
            }

        }
        else {
            this.response.speak("say the score")
                .listen(' ');
            this.emit(':responseReady');
        }
    },

    'AMAZON.NoIntent': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    'AMAZON.YesIntent': function () {
        var l = this.attributes.r;
        if (l == 2) {
            this.handler.state = states.POSTMATCH;
            this.emitWithState('Start');
        }
        else {
            this.emitWithState('Start');
        }
    },

    'AMAZON.HelpIntent': function () {
        this.response.speak('Lets have a game of book cricket, say yes to start the game')
            .listen('say yes to start the game');
        this.emit(':responseReady');
    },

    'AMAZON.StopIntent': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    'AMAZON.CancelIntent' : function (){
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    }, 

    'SessionEndedRequest': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    }
});
//PostMatch
const PostmatchHandler = Alexa.CreateStateHandler(states.POSTMATCH, {
    "LaunchRequest": function () {
        this.emitWithState('Start');
    },
    "Start": function () {
        var g = this.attributes.Total;
        var h = this.attributes.Total2;
        if (g > h) {
            var b = g - h;
            var imaged = images.imagepost();;
            const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
            const template = builder.setBackgroundImage(makeImage(imaged))
                .setBackButtonBehavior('HIDDEN')
                .build();
            this.response.renderTemplate(template)
                .speak('The bowling team have restricted the batting team to a score of ' + h + ' and won the match by ' + b + 'runs. Congratulations')
                .listen('say yes to play again else say no')
                .shouldEndSession(true);
            this.emit(':responseReady');

        }
        else {
            var imaged = images.imagepost();;
            const builder = new Alexa.templateBuilders.BodyTemplate7Builder();
            const template = builder.setBackgroundImage(makeImage(imaged))
                .setBackButtonBehavior('HIDDEN')
                .build();
            this.response.renderTemplate(template)
                .speak('The batting team has successfully chased the target and won the match. Congratulations. The Final score of Team 2 is' + h + ' while the Team 1 scored' + g + 'runs.')
                .listen('Say yes to play again else say no')
                .shouldEndSession(true);
            this.emit(':responseReady');
        }
    },
    'AMAZON.NoIntent': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    'AMAZON.YesIntent': function () {
        this.handler.state = states.GAMESTART;
        this.emitWithState('Start');
    },
    'Unhandled': function () {
        this.emit(':ask', "Please say yes or no ", "You need to answer with a yes or no");
    },

    'AMAZON.HelpIntent': function () {
        this.response.speak('Lets have a game of book cricket, say yes to start the game')
            .listen('say yes to start the game');
        this.emit(':responseReady');
    },

    'AMAZON.StopIntent': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    'AMAZON.CancelIntent' : function (){
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    }, 

    'SessionEndedRequest': function () {
        this.response.speak('Thanks for playing. Hope to see you again.')
            .shouldEndSession(true);
        this.emit(':saveState', true);
        this.emit(':responseReady');
    }
});
