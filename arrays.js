

// Out responses//

var Out = ["It's miles in the air. It's a wonderful catch. What a catch!. The greatest catch!. Unbelievable.",
    "Oh! What has he done. That’s a hit wicket. Tried to glance the ball down towards fine leg, while the trailing leg glanced the bails. Comedy of a error.",
    "Batsmen whips the ball straight to the midwicket fielder , and tries to take a cheeky single but a direct hit and that's what the bowling team wanted.",
    "Batsmen plays it defensively and tries to take a quick single. Direct hit from the fielder and the batsmen can continue running into the dressing room.",
    "good delivery from the bowler, beating the outside edge of batsmen and through to the keeper.Bowling team strike once again at the vital moment.",
    "OUT, this one reaches long leg! A short-of-a-length ball, Batsmen goes for the pull, goes from down to up, gets a big top edge which is comfortably held.",
    "OUT, taken at long leg! Bowler sets off on a sprint! He's broken the match wide open! Batsman picks out the fielder to perfection.",
    "OUT, sensational catch! At long-off, barely an inch away from the boundary line! Fielder, take a bow!",
    "OUT, hits him on the half-volley as he charges out of his crease and it's given lbw!",
    "OUT, hit straight to the fielder at mid-off"
];

// 1 run //
var Onerun = ["Bludgeoned through the covers, but straight to the fielder. Have to settle down for a single.",
    "Batsmen cuts the ball through the point and takes a single",
    "Batsmen whips the ball on the onside and takes a single",
    "Batsmen taps the ball straight to the mid-on fielder and takes a single.",
    "Batsmen plays it straight to the deep mid-on and takes a single",
    "excellent delivery from the bowler, hits batsmen's pads. Unfortunately, batsmen crosses over for a single.",
    "1 run, short of a length, angled in at the body, flicked off the hip to deep square",
    "1 run, good length outside off bowled at 137 kilometers per hour, angled down to third man",
    "1 run, shuffles across the crease and works a length delivery from around off stump through midwicket",
    "1 run, full outside off, drives down the ground...signs it's becoming harder to time the ball",
    "1 run, stays leg-side of the ball, drives into the covers off the toe end of the bat",
    "1 run, fuller delivery, angled in at batsmen who again moves to the leg side, pushes it straight back past the bowler",
    "1 run, a slower ball, dragged down outside leg stump, batsmen plays a front-foot pull which nearly carries to deep square",
    "1 run, gets solidly forward into a good-length ball, timed nicely into the covers but a diving save keeps it to a single",
    "1 run, goes back to a flatter delivery on middle and tucks it to deep square leg",
    "1 run, inside edge as batsman is rooted to the crease, attempting to punch a 143 kph delivery, shortish in length, through cover. There is however not enough room for that",
    "1 run, fumble at cover, with the ball coming straight at him, allows batsman to get off the mark straight away",
    "1 run, back of a length outside off, tapped to point",
];


// 2 runs //
var Tworun = ["Glanced down the fine leg for 2 runs",
    "Pushed through the covers. Batsmen are running hard and successfully complete 2 runs",
    "Batsmen taps the ball on the onside and takes two quick runs",
    "2 runs, another slower one outside off, not much timing as he drives into the covers but that lack of pace on the ball actually helps him get back for a second",
    "2 runs, touch of swing from a good length, into the pads, flicked through square leg",
    "2 runs, back of a length around off, swatted past midwicket for a couple. A forehand shot",
    "2 runs, a slower ball around middle, clipped to the fielder, who hits the stumps this time and concedes an extra run as the ball ricochets away",
    "2 runs, back of a length outside off, a thick inside edge past the stumps for a couple to fine leg.",
    "2 runs, keeps back to a good length ball, waits until it is right under his eyes, whips it to the right of midwicket and finds a couple before the men in the deep can do much",
    "2 runs, mistimes the pull . Manages to sneak it past the fielder at square leg though",
    "2 runs, slightly leg side-ish and that's enough for the batsman to flick through square leg for a couple.",
    "2 runs, back of a length outside off, opens the face, steers it wide of third man and steals the second",
    "2 runs, looks like a miscued shot. Looks to slap this short ball through the covers but it sticks in the pitch and he ends up hitting it over mid-off.",
    "2 runs, full delivery outside off, batsman shuffles across and comes forward and flicks the ball through midwicket. No man there, two more"
];


// 3 runs // 

var Threerun = ["Pulled thorugh the midwicket region for 3 runs",
    "3 runs, full outside off, clubbed through the leg side.",
    "3 runs, touch of swing from a good length, into the pads, flicked through square leg"
];


// 4 runs //

var Fourrun = ["four runs , he has blazed that one through the off-side field. Go and fetch that!",
    "Terrific shot. Took advantage of the mid off and lifted over him for 4 runs. No chance for the fielder.",
    "Textbook straight drive. Bowler had no chance. That’s 4 runs.",
    "Wide on the pads and the batsman whips the ball to the boundary. A welcome boundary for the batting team",
    "FOUR runs, a slower ball outside off, driven firmly into the covers where the fielder dives over the ball , and it beats the sweeper",
    "FOUR runs, gets this through the off side, it's chased hard and flicked back...but was it before touching the rope? No, hand touching ball and rope together. Good effort, though, by the fielder",
    "FOUR runs, hammered! Gives himself a touch of room to the leg side and then flays this.",
    "FOUR runs, pumped down the ground! Batsman looking good. Bowler goes full at off stump, batsmen strides forward and drives straight of mid-off",
    "FOUR runs, on leg stump and another strong leg-side shot from batsmen, whips it off the pads behind square.",
    "FOUR runs, that's brilliantly done! Bowler went short, was a straight delivery, Batsmen arches back and ramps it straight over the keeper",
    "FOUR runs, gets onto the pads, very full, sweetly clipped in the air through midwicket",
    "FOUR runs, cracking shot, gets forward to a full ball outside off and drives it straight down the ground...just has the legs to reach the rope",
    "FOUR, when things are going your way, it just goes your way. A short delivery outside off, Batsmen shapes and executes a reverse-sweeps past short third man for four.",
    "FOUR, good length outside off, batsmen cuts the ball down to third man.  Fielder gets across to his right, stumbles, fumbles and lets the ball through to the third man boundary.",
    "FOUR, batsmen has taken on an off-side wide, got right across and pulled it over square leg.",
    "FOUR, superb batting . Bowler thought he'll play the same lap sweep but he hits it the other way, the reverse sweep, full around off, fantastic timing over point. Just inside the boundary too.",
    "FOUR, good execution. Full delivery, very quick around off. Batsman brings out the cross-batted heave. The bat met the ball at an awkward angle but he would not care. Picks the gap at midwicket, fierce hit.",
    "FOUR, that's over point. Four more. Back of a length, slightly wide outside off, Batsman gets underneath the length to go from bottom to top, ensuring the ball went over point. Very clever thinking.",
    "FOUR, lovely hit from Batsman. A full delivery outside off, in the slot and batsman lofts the ball over mid-off, an extension of a drive. Excellent shot",
    "FOUR, outside edge but it splits the gap between keeper and wide slip",
    "FOUR, delicious batting. Batsman spots the back of a length delivery outside off. He knows third man is up. He knows the keeper is up as well. So he dials up the perfect little late cut",
    "FOUR, picks the length early, stays back and pulls imperiously. Back of a length on middle and leg, Batsman gets back and nails a pull over square leg",
    "FOUR runs, comes down and chips the ball over mid-on. The fielder is in so free runs",
];


// 5 runs //

var Fiverun = ["A no ball and a four, tossed up outside off, reaches it on the full and slices it behind square",
    "FOUR runs of a no ball, inside edge past leg stump and well wide of leg gully",
    "FOUR runs, gorgeous from the batsman. Length ball outside off and it's a clean drive on the up, wide of mid-off. Also, a bonus run for a no ball"
];



// 6 Runs //
var Sixrun = ["The little man has hit the big fella for a six! He's half his size!",
    "What a biggie! It's gone into the trees. Six runs",
    "That's a great shot. What a little beauty! That's gone miles over the top of midwicket for a six.",
    "That was a loose delivery. Batsmen jumps out and hits over the point for a huge six.",
    "SIX runs, that breaks the shackles, brings out the slog sweep over deep midwicket, goes down the back knee and connects sweetly",
    "SIX, all that adrenaline is still there, some flight around off, batsmen stays low and slog-sweeps the ball over midwicket for six.",
    "SIX, bowler goes short, and it's so slow that batsmen rocks back, picks the length and says thank you very much. He gives the ball a good smack, way over midwicket. Sweet off the bat",
    "SIX, out of here. Goes quick, flat outside off, Batsmen gets underneath it and sends it a million miles away, sensational connection and way over long-on. That is massive.",
    "SIX, and then batsmen goes wheee, right in his wheelhouse. Flat and quick, in the arc, and well, it's out of here. Smashed over midwicket. The stadium is buzzing.",
    "SIX, launches it over square leg. Thunders down the pitch to take a length ball on leg stump and flicks majestically over the boundary. Splendid cricket.",
    "SIX runs, over deep midwicket!. Bowler went back of a length with a cutter, batsman stayed deep in his crease and pulled it powerfully.",
    "SIX runs, oh dear how has he hit it there?! Wow! Short ball outside off, gets on top of the bounce and gets his legs positioned like we used to see Kallis do, with the front leg coming across the body"
];


// NO runs //

var zerorun = ["Beautiful angling delivery from bowler. Good defence by the batsman",
    "no run, gets forward into a length ball, slightly slower at 122 kilometer per hour, pushed to cover",
    "no run, bowler does well here, gets his right hand down to intercept a firm straight drive as batsmen latches onto a full delivery",
    "no run, full outside off, left alone",
    "no run, takes a wild risk as he lunges forward, looking for a reverse swat. He's so off balance that he falls on his back side. Needless to say he mistimes the shot to short third man",
    "past the edge as batsman attempts a cut and is surprised by a lot of extra bounce",
    "no run, closer to off this time, 140 kilometers per hour, batsman is behind the line as he blocks.",
    "no run, good lines from the bowler, in a channel just outside off, defended into the mid-off region",
    "no run, full delivery outside off, seam movement, defended back.",
    "no run, overpitched outside off, batsman leans forward and drills a drive straight into the hands of cover",
    "no run, massive lbw appeal but the umpire is unmoved. Could have pitched outside leg, and an inside edge as well.",
    "no run, beats him with the full delivery, darting past the outside edge. Batsman is drawn into a drive and is done by the seam movement",
    "no run, the drive comes out, looking to beat point to his right, but the fielder has been excellent in that region",
    "no run, short and wide outside off, left alone.",
    " no run, good length delivery outside off, a more decisive leave from batsman."
];
module.exports = {
    'answer0': function () {
        var answer0 = Out[Math.floor(Math.random() * Out.length)];
        return answer0;
        
    },
    'answer1': function () {
        var answer1 = Onerun[Math.floor(Math.random() * Onerun.length)];
        return answer1;
    },
    'answer2': function () {
        var answer2 = Tworun[Math.floor(Math.random() * Tworun.length)];
        return answer2;
    },
    'answer3': function () {
        var answer3 = Threerun[Math.floor(Math.random() * Threerun.length)];
        return answer3;  
    },
    'answer4': function () {
        var answer4 = Fourrun[Math.floor(Math.random() * Fourrun.length)];
        return answer4;
    },
    'answer5': function () {
        var answer5 = Fiverun[Math.floor(Math.random() * Fiverun.length)];
        return answer5;
    },
    'answer6': function () {
        var answer6 = Sixrun[Math.floor(Math.random() * Sixrun.length)];
        return answer6;
    },
    'answernil': function () {
        var answernil = zerorun[Math.floor(Math.random() * zerorun.length)];
        return answernil;
    },
}